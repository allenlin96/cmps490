<html>
<head>
<title>Ticketing System</title>
</head>

<body>
<?php
include_once("NavIndex.php");
?>

<div class="pimg2">

<!-- Container Class -->
<div class="loginBoxRegister">

<img src="loglogo.jpg" class="avatar">
<form class="form-horizontal" action = "RegisterProcess.php">
<fieldset>


<!-- Form Name -->
<legend>Register</legend>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="UserName">USERNAME</label>  
  <div class="col-md-4">
  <input id="UserName" name="UserName" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Password">PASSWORD</label>  
  <div class="col-md-4">
  <input id="Password" name="Password" type="password" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Fname">FIRST NAME</label>  
  <div class="col-md-4">
  <input id="Fname" name="Fname" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Lname">LAST NAME</label>  
  <div class="col-md-4">
  <input id="Lname" name="Lname" type="text" placeholder="" required="True"class="form-control input-md">
    
  </div>
</div>




<button type="Submit" name="Submit" class="btn btn-primary">Register</button>


</fieldset>
</form>
<a href="index.php">Click here to return Home</a>



</div>
</div>
</body>
</html>
