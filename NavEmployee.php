<html>     
<head>   
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>
Ticketing System
</title>
<style type=text/css>

.table {
  border-collapse: collapse;
  width: 100%;
  
}

td{
	text-align:left;
	padding: 10px;
	align:left;
	padding-left:120px;
}

th {
  text-align: center;
  padding: 10px;
  align:left;
  padding-left:125px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color:#0258b4;
  color: white;
  text-align:center;
}











body
{
height: 125vh;
margin-top: 80px;
padding: 30px;
background-size: cover;
background-color: White;
font-family: sans-serif;
color: #002A4E;
}

header {
background-color:  #0258b4;
position: fixed;
left: 0;
right: 0;
top: 0px;
height: 40px;
display: flex;
align-items: center;
box-shadow: 0 0 5px 0 black;
padding:50px;
}

img{
	float:left;
	width: 150px;
    height: auto;
	cursor: pointer;
}


.container{
	background:#D4E6F1;
	margin: auto;
  	padding:50px;
    width:2000px;
	
}



header * {
display: inline;
}

a {
color: white;
}

header li {
margin: 12px;

}


header li a {
color: white;
text-decoration: none;
}


a:hover{
	background-color: #01346c;
}


input {
background-color: #B2DBFF;

}


.btn{
    background-color: #0258b4; 
    border: none;
    color: white;
    padding: 15px 32px;
    text-decoration: none;
    display:inline-block;
    font-size: 15px;
    cursor: pointer;
    float:left;
    font-family: Arial, Helvetica, sans-serif;
  }




.btn:hover {
    background-color: #01346c;
  }
  




</style>
</Head>
<body>
	<header>
		
		<nav>
		<ul>

			<li>
				<a href="EmployeeAccountPage.php"> <img src="Logo.png" alt="Logo"></a>
				
			</li>
			
			<li>
				<a href="EmployeeAccountPage.php" class= "btn btn-primary"> Home Page </a>
				
			</li>
			<li>  
				<a href="Logout.php" class= "btn btn-primary">Logout </a>  
			</li>
		</ul>
		</nav>
	</header>
</body>
</html>
<?php
?>
