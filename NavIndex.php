<html>     
<head>   
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<title>
Ticketing System
</title>


<style type=text/css>


 img {
    float:left;
	  width: 250px;
    height: auto;
	  cursor: pointer;
  }


.container {
  z-index:2;
  background:#0258b4;
  color:white;
  padding: 50px;
  max-width: 540px;
	font-family: Arial, Helvetica, sans-serif;
  margin: auto;
  border-radius:2%;
  border-color:none;
  box-shadow: 0 0 5px 1px black;
}


.btn{
    background-color: #0258b4; 
    border: none;
    color: white;
    padding: 15px 32px;
    text-decoration: none;
    display:inline-block;
    font-size: 19px;
    cursor: pointer;
    font-family: Arial, Helvetica, sans-serif;
    float:right;
    
   }


.indextext{
  position: absolute;
  line-height: 60px;
  top: 0.2%;
  margin-left:80px;
  width: 900px;
  text-align: center;
  color:White;
  font-style: oblique;
  font-size: 40px;
  font-weight:bold;
  letter-spacing: 9px;
  text-transform: uppercase;
  padding:10px;
}


   .btn:hover {
   background-color: #01346c;
 }


#btn-group{
  float:right inline;
  
}

.obtns{
  float:left;
}

body, html {
  height: 100%;
  font-size: 18px;
  font-family: Arial, Helvetica, sans-serif;
  font-weight: 400;
  line-height: 1.8em;
  color: white;
}

.pimg1, .pimg2, .pimg3, .pimg4{
  position: relative;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-attachment: fixed;
}

.pimg1{
  background-image: url(22.jpg);
  min-height: 100%;
}

.pimg2{
  background-image: url(ip.jpg);
  min-height: 100%;
}

.pimg3{
  background-image: url(coni.jpg);
  min-height: 100%;
}

.pimg4{
  background-image: url(11111.jpg);
  min-height: 100%;
}

.section{
  text-align: left;
  padding: 10px 20px;
 
}

.section-light{
  background-color: #0258b4;
  color: white;
}

.section-dark{
  background-color: #0258b4;
  color:white;
}

.p1text{
  position: absolute;
  top: 50%;
  width: 100%;
  text-align: center;
  color: #000;
  font-size: 27px;
  letter-spacing: 8px;
  text-transform: uppercase;
}

.botban{
  align:left;
  font-size:15px;
  margin:25px;
  font-style: italic;
}


.avatar{
  width: 100px;
  height: 100px;
  border-radius: 50%;
  position: absolute;
  top: -50px;
  left: calc(50% - 50px);
}



.loginBoxRegister {
  width:450px;
  height:500px;
  background:#000;
  color: #ffff;
  top: 50%;
  left: 50%;
  position: absolute;
  transform: translate(-50%, -50%);
  box-sizing: border-box;
  padding: 70px 30px;
  box-shadow: 5px 5px 5px  black;
}


.loginBox {
  width:390px;
  height:450px;
  background:#000;
  color: #ffff;
  top: 50%;
  left: 50%;
  position: absolute;
  transform: translate(-50%, -50%);
  box-sizing: border-box;
  padding: 70px 30px;
  box-shadow: 5px 5px 5px  black;
}



.loginEmployeeBox {
  width:390px;
  height:450px;
  background:#000;
  color: #ffff;
  top: 50%;
  left: 50%;
  position: absolute;
  transform: translate(-50%, -50%);
  box-sizing: border-box;
  padding: 70px 30px;
  box-shadow: 5px 5px 5px  black;
}


.loginBox label{
  margin: 0;
  padding: 0;
  font-weight: bold;
}

.loginBox input{
  width: 100%;
  margin-bottom: 20px;
}

.loginBoxRegister .btn[type="submit"]{
  border: none;
  outline: none;
  height: 40px;
  background: #0258b4;
  color: #fff;
  font-size: 20px;
  border-radius: 20px;
  width:100%;
  text-align:center;
  padding:7px;
  
}

.loginBox .btn[type="submit"]{
  border: none;
  outline: none;
  height: 40px;
  background: #0258b4;
  color: #fff;
  font-size: 20px;
  border-radius: 20px;
  width:100%;
  text-align:center;
  padding:7px;
}




.loginEmployeeBox .btn[type="submit"]{
  border: none;
  outline: none;
  height: 40px;
  background: #0258b4;
  color: #fff;
  font-size: 20px;
  border-radius: 20px;
  width:100%;
  text-align:center;
  padding:7px;
}




.loginBoxRegister a{
  text-decoration: none;
  font-size: 15px;
  line-height: 22px;
  color: rgb(226, 226, 226);
}




.loginEmployeeBox a{
  text-decoration: none;
  font-size: 15px;
  line-height: 22px;
  color: rgb(226, 226, 226);
}


.loginBox a{
  text-decoration: none;
  font-size: 15px;
  line-height: 22px;
  color: rgb(226, 226, 226);
}


.loginBox .btn:hover{
  background-color: #01346c;
}

.loginBoxRegister .btn:hover{
  background-color: #01346c;
}


.loginEmployeeBox .btn:hover{
  background-color: #01346c;
}


.loginBox a:hover{
  color: #0258b4;
}


.loginBoxRegister a:hover{
  color: #0258b4;
}

.loginEmployeeBox a:hover{
  color: #0258b4;
}

legend{
  color:white;

}

.loginBoxRegister .col-md-4{
  width:47%;
}


.serv1, .serv2, .serv3, .serv4{
  position: relative;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-attachment: fixed;
}

.serv1{
  background-image: url(11111.jpg);
  min-height: 100%;
}

.serv2{
  background-image: url(coni.jpg);
  min-height: 100%;
}

.serv3{
  background-image: url(2.jpg);
  min-height: 100%;
}

.serv4{
  background-image: url(4.jpg);
  min-height: 100%;
}

.rightMap{
  float:right;
  top: 30%;
}






</style>

</Head>
<body>
	<header>
		<nav>
		</nav>
	</header>
</body>
</html>
<?php
?>