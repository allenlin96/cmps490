:: Uncomment first line to reset database
c:\xampp\mysql\bin\mysql -h localhost -u root -e "DROP DATABASE IF EXISTS cmps490db;"

:: Creating database
c:\xampp\mysql\bin\mysql -h localhost -u root -e "CREATE DATABASE IF NOT EXISTS cmps490db;"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; CREATE TABLE IF NOT EXISTS Users(UserId INT PRIMARY KEY AUTO_INCREMENT, UserName VARCHAR(20) UNIQUE, Password VARCHAR(20) UNIQUE, FName VARCHAR(20), LName VARCHAR(20));"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; CREATE TABLE IF NOT EXISTS Complaints(ComplaintId INT PRIMARY KEY AUTO_INCREMENT, Location TEXT, City TEXT, State TEXT, ZIP INT, Complaint TEXT, Description TEXT, UserId INT DEFAULT 0, Status Text);"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; CREATE TABLE IF NOT EXISTS Workers(WorkerId INT KEY AUTO_INCREMENT,UserName VARCHAR(20) UNIQUE, Password VARCHAR(20) UNIQUE, FName VARCHAR(20), LName VARCHAR(20));"

:: Inserting data
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Users (UserName, Password, FName, LName) VALUES('user1', 'pass1', 'John', 'Smith');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Users (UserName, Password, FName, LName) VALUES('user2', 'pass2', 'Mary', 'Jane');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Users (UserName, Password, FName, LName) VALUES('user3', 'pass3', 'James', 'Roy');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Complaints (Location, City, State, ZIP, Complaint, Description, UserId, Status) VALUES('ULL Campus near oliver hall','Lafayette','Louisiana','70501','Road','big pothole near the front of oliver hall', '1', 'active');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Complaints (Location, City, State, ZIP, Complaint, Description, UserId, Status) VALUES('ULL Campus near oliver hall','Lafayette','Louisiana','70501','Road','tree fell down due to storm', '2', 'active');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Workers (UserName, Password, FName, LName) VALUES('worker1', 'pass1', 'James', 'Franklin');"
c:\xampp\mysql\bin\mysql -h localhost -u root -e "USE cmps490db; INSERT INTO Workers (UserName, Password, FName, LName) VALUES('worker2', 'pass2', 'Allen', 'Lin');"