<?php
session_start();
include_once("NavEmployee.php");
include "dbconnect.php";
if($mysqli->connect_errno) {
	echo "Failed to connect to MySQL: ( " . $mysql->connect_errno. " ) ". $mysql->conn_error;
}
$UserId = $mysqli -> real_escape_string($_GET["UserId"]);
//$currentWorkerId = $_SESSION['WorkerId'];
$sql = " SELECT * FROM Complaints WHERE Status = 'active'  AND UserId = '$UserId'"; 
$result = $mysqli->query($sql);
?>


<html>
<body>
<div class="container">
<?php
echo "<b>Showing all complaints for User $UserId<b>";
//output data as table
echo "<h3>All Active Complaints </h3>";

	echo "<table style='border: solid 2px black;'>
	<tr>
	    <th>Complaint ID </th>
	    <th>Complaint Description</th>
		<th>Location</th>
		<th>City</th>
		<th>State</th>
		<th>ZIP</th>
		<th>Complaint Type</th>
		<th>UserId</th>
		<th>View</th>
	</tr>";

	while ($row = $result -> fetch_assoc()){
		echo 
		'<tr>
			<td style="width: 200px;" text-align: left;> '.$row['ComplaintId'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['Description'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['Location'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['City'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['State'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['ZIP'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['Complaint'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['UserId'].' </td> 
			<td style="width: 1000px;" text-align: left;>
				<form class="form-row align-items-center" action = "CompleteComplaint.php">
				<input id="ComplaintId" name="ComplaintId" type=hidden value='.$row['ComplaintId'].' class="form-control input-md">
				<input type=submit name=submit value=Complete>
				</form>
			</td>
			
		</tr>';	
	}
	
$sql = " SELECT * FROM Complaints WHERE Status = 'completed' AND UserId = '$UserId' "; 
echo "</table><br><br>";
$result2 = $mysqli->query($sql);
?>

</div>
</html>

<html>

<div class="container">

<?php
//output data as table
echo "<h3>All Completed Complaints</h3>";

	echo "<table style='border: solid 2px black;'>
	<tr>
	    <th>Complaint ID </th>
	    <th>Complaint Description</th>
		<th>Location</th>
		<th>City</th>
		<th>State</th>
		<th>ZIP</th>
		<th>Complaint Type</th>
		<th>UserId</th>
		
	</tr>";
	
	while ($row = $result2 -> fetch_assoc()){
		echo 
		'<tr>
			<td style="width: 200px;" text-align: left;> '.$row['ComplaintId'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['Description'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['Location'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['City'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['State'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['ZIP'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['Complaint'].' </td>
			<td style="width: 1000px;" text-align: left;> '.$row['UserId'].' </td>
		</tr>';	
	}
	

?>

</body>
</html>
