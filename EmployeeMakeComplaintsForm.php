<html>
<head>
<title>Ticketing System</title>
</head>

<body>
<?php
include_once("NavEmployee.php");
?>



<!-- Container Class -->
<div class="container">


<form class="form-horizontal" action = "EmployeeMakeComplaintProcess.php">
<fieldset>


<!-- Form Name -->
<legend>Complaint Form</legend>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Location">Location</label>  
  <div class="col-md-4">
  <input id="Location" name="Location" type="text" placeholder="" required="True" class="form-control input-md" size="50">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="City">City</label>  
  <div class="col-md-4">
  <input id="City" name="City" type="City" placeholder="" required="True" class="form-control input-md" "20">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="State">State</label>  
  <div class="col-md-4">
  <input id="State" name="State" type="text" placeholder="" required="True" class="form-control input-md" "20">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ZIP">ZIP</label>  
  <div class="col-md-4">
  <input id="ZIP" name="ZIP" type="INT" placeholder="" required="True"class="form-control input-md" size="5">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Complaint">Complaint Type</label>  
  <div class="col-md-4">
  <input id="Complaint" name="Complaint" type="text" placeholder="" required="True"class="form-control input-md">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Description">Description</label>  
  <div class="col-md-4">
  <textarea id="Description" name="Description" type="text" placeholder="" required="True"class="form-control input-md">
  </textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Submit"></label>
  <div class="col-md-4">
    <button id="Submit" name="Submit" class="btn btn-primary">File Complaint</button>
  </div>
</div>
</fieldset>
</form>


<form class="form" action = "EmployeeAccountPage.php">	
		<button type="submit" class="btn btn-primary">Back</button>
</form>

</div>
</body>
</html>
