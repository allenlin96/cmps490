<html>
<head>
	<title>Ticket System Home</title>
	
<link rel="icon" href="tablogo.png" type="image/ico">

</head>

<body>
<?php
session_start();
include_once("NavIndex.php");
include "dbconnect.php";
?>


 <div class="pimg1">
<img src="Logo.png" alt="Logo">
    <div id="btn-group" role="group">
        
	<form class="form" action = "services.php">
			<button class="btn btn-primary">Services</button>
	</form>

	<form class="form" action = "RegisterForm.php">
         	<button type="submit" class="btn btn-primary">Register</button>
	</form>
	
	<form class="form" action = "LoginPage.php">
          	<button type="submit" class="btn btn-primary">Sign In</button>
	</form>
	
	<form class="form" action = "EmployeeLoginPage.php">
          	<button type="submit" class="btn btn-primary">Employee Login</button>
    </form>

    </div>
    <div class="indextext">
      <span class="border trans">
        City of New Iberia Goverment Services
      </span>
    </div>
  </div>

<section class="section section-dark">
		<div class="botban">
      <h3> Contact Us!</h3>

			<div class="rightMap">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27645.281567502865!2d-91.84594300963319!3d29.98919414173928!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8623828b501543b9%3A0x565f46b25de0fdb5!2sCity%20of%20New%20Iberia%20City%20Hall!5e0!3m2!1sen!2sus!4v1649390718150!5m2!1sen!2sus" width="350" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
			</div>

      	<br>City of New Iberia
      	<br>457 East Main Street, Suite 300. New Iberia, LA. 70560
      	<br>Phone: (337)123-4567
	  	  <br>Fax: (337)789-1011
        <br>Email: contactinfo@cityofnewiberia.gov
        <br>Office Hours: Monday - Friday: 8am - 5pm<br>

    </div>
</section>



<?php
$mysqli->close();
?>
</body>
</html>