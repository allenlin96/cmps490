<html>
<head>
<title>Ticketing System</title>
</head>

<body>
<?php
include_once("NavEmployee.php");
?>

<div class="container">

  <legend>Search all complaints by UserId</legend>
  <form action = "EmployeeSearchComplaintsProcess.php">
  <div class="form-group">
    <label for="UserId">UserId</label>
    <input id="UserId" name="UserId" type="text" placeholder="Enter UserId" required="True" class="form-control input-md">
  </div> 
    <button type="submit" class="btn btn-primary">Search</button>
  </form>
</div>

</body>
</html>
